/*
 * This Java source file was generated by the Gradle 'init' task.
 */
public class Pila {
	private int tope, maximo;
	private String pila[];
	private boolean status;
	
	public Pila() {
	}
	
	public Pila(int m) {
		status=verificarMaximoIngresado(m);
		if(status==true) {
		maximo=m;
		pila=new String[maximo];
		tope=0;
		}
	}
	
	public boolean PilaCreada() {
		return status;
	}
	
	public int buscar(String buscado) {
		while(VerificarVacia()==false){
			  String datoAlmacenado=pila[tope-1];
			  if(datoAlmacenado==buscado) {
				  return tope-1;
			  }
			  tope--;
			}
		  return -1;
	}
	
	public boolean verificarMaximoIngresado(int m) {
		if(m>0) {
		return true;
		}else {
			return false;
		}
	}
	
	public int retornaTamano (){
		return maximo;
	}
	
	public int retornaNumeroElementos() {
		return tope;
	}

	public String push(String texto){
		  if(tope<maximo){
		   pila[tope]=texto;
		   tope++;
		   System.out.println("Dato agregado a la pila");
		   return "Dato agregado a la pila";
		  }else{
		   System.out.println("No se puede agregar,pila llena");
		   return "No se puede agregar,pila llena";
		  }
	}

	public String pull(){
	  if(tope>0){
	  tope--;
	  System.out.println("Dato extraido");
	  return "Dato extraido";
	  }else{
	  System.out.println("Ya no se pueden sacar datos, pila vacia");
	  return "Ya no se pueden sacar datos, pila vacia";
	  }
	}
	
	public String top(){
	  if(VerificarVacia()==false){
			  return pila[tope-1];
	  }else {
		  return "Pila Vacia";
	  }
	 }

	public String pop() {
		  if(VerificarVacia()==false){
			  String datoAlmacenado=pila[tope-1];
			  tope--;
			  return datoAlmacenado;
			  
	  }else {
		  return "Pila Vacia";
	  }
	}
	
	public boolean VerificarVacia(){
	  if(tope==0) 
		  return true;
	  else 
		  return false;
	 }
}
